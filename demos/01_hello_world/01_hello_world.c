/*
 * Copyright (C) 2019  Joseph Marshall
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "lib2d.h"
#include <stdio.h>
#include <SDL2/SDL.h>

int main(int argc, char** argv) {
    SDL_Init(SDL_INIT_VIDEO);
    SDL_Window* window = SDL_CreateWindow("01_hello_world",
            SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
            800, 800, SDL_WINDOW_SHOWN|SDL_WINDOW_OPENGL|SDL_WINDOW_RESIZABLE);
    SDL_GL_CreateContext(window);

    if (lib2d_init(LIB2D_RENDER_BACKEND_GL, 0) != 0) {
        fprintf(stderr, "%s\n", lib2d_get_error());
        return -1;
    }
    lib2d_viewport(800, 800);

    lib2d_drawable d = {.x=200, .y=200, .w=128, .h=128, .color=0x000000ff};
    lib2d_drawable d2 = {.x=0, .y=0, .w=128, .h=128, .color=0xff00008f};

    SDL_Event event;
    char running = 1;
    while (running) {
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
                case SDL_QUIT:
                    running = 0;
                    break;
                case SDL_WINDOWEVENT:
                    switch (event.window.event) {
                        case SDL_WINDOWEVENT_SIZE_CHANGED:
                            lib2d_viewport(event.window.data1, event.window.data2);
                            break;
                    }
                    break;
                case SDL_MOUSEMOTION:
                    d2.x = event.motion.x - 64;
                    d2.y = event.motion.y - 64;
                    break;
            }
        }

        lib2d_clear(1,1,1,1);
        d.rot += 0.01f;
        lib2d_draw(&d);
        lib2d_draw(&d2);

        lib2d_render();
        SDL_GL_SwapWindow(window);
    }
    lib2d_shutdown();
    SDL_DestroyWindow(window);
    SDL_Quit();
    return 0;
}

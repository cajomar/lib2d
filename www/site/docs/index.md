template:docs.html
title:Lib2D Documentation

Lib2D is an open source, cross-platform rendering library for programmers making games.
Its main feature is efficiently rendering sprites using a variety optimisations such as automatic texture atlasing and render batching.

Lib2D is implemented in C with first-class Python language bindings.

# API Docs

{% for l in langs %}
* ##[{{ l.lang.name }}]({{ docs_url }}/{{ l.lang.id }}/)
{% endfor %}


# Tutorials

[Emscripten, SDL2, and Lib2D]({{ docs_url }}/emscripten-sdl2-lib2d)

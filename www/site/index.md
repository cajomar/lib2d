template:site.html
title:Lightweight 2D Game Library

* Draws sprites to the screen.
* Immediate mode API.
* It's pretty darn fast.
* Automatic texture atlasing.
* Automatic draw call batching.
* [Draws text](/docs/c/cls-lib2d_font/).
* Nice [Python API](/docs/python/).
* Nice [C API](/docs/c/).
* [Open source](https://bitbucket.org/jlm/lib2d/) licensed under the MIT.

# Getting Started

For python, install using pip:

```
$ sudo python3 -m pip install lib2d
```

For C users download the [source code](https://bitbucket.org/jlm/lib2d/) or use git and build manually:

```
$ git clone https://jlm@bitbucket.org/jlm/lib2d.git
$ cd lib2d
$ mkdir build; cd build
$ cmake ..
$ make -j && sudo make install
```

The `demos` folder contains some projects to help get you started.

# Hello World

``` Python
pygame.display.init()
pygame.display.set_mode((800, 600), HWSURFACE|OPENGL|DOUBLEBUF)
glClearColor(1.0, 1.0, 1.0, 0.0)

lib2d.init()
lib2d.viewport(800, 600)

sprite = lib2d.Drawable("image.png", x=200, y=200)

while True:
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    sprite.draw()
    lib2d.render()
    pygame.display.flip()
```
``` C
SDL_Init(SDL_INIT_VIDEO);
SDL_Window* window = SDL_CreateWindow("Hello World",
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
        800, 600, SDL_WINDOW_OPENGL);
SDL_Renderer* renderer = SDL_CreateRenderer(window, -1,
        SDL_RENDERER_ACCELERATED|SDL_RENDERER_PRESENTVSYNC);
SDL_SetRenderDrawColor(renderer, 0xff, 0xff, 0xff, 0xff);

lib2d_init(LIB2D_RENDER_BACKEND_GL, 0);
lib2d_viewport(800, 600);

lib2d_image im;
lib2d_image_info info;
lib2d_image_load("image.png", &im, &info);
lib2d_drawable sprite = {.image=im, .w=info.w, .h=info.h,
    .x=300, .y=300, .color=0xffffffff};

while (1) {
    SDL_RenderClear(renderer);
    lib2d_draw(&sprite);
    lib2d_render();
    SDL_RenderPresent(renderer);
}
```

# Roadmap

The core of lib2d is meant to be ludicrously simple in scope, with optional extensions providing additional functionality.
Some planned extensions are:

* **lib2d_animation** - A convenient way to animate the various ``Drawable`` members.
* **lib2d_tile** - A more efficient way to render a tile map that renders with triangle strips and ensures batching isn't broken by textures. Possibly direct tiled support as well.
* **lib2d_effects** - For rendering beyond simple sprites by using more advanced shaders.

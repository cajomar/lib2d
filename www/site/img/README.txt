Place images in this folder. Use in templates like such:

!thumb[alt text](image_name size:400)#image_id
!nothumb[](image_name)

results in something like:

<a href="/site/img/image_name.png">
    <img src="/site/img/thumb/image_name_800.png" alt="alt text" id="image_id" width=800 height=260 class="thumb">
</a>

<img src="/site/img/image_name.png" width=1600 height=520>

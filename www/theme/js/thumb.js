
var toggle_thumb = function() {
    if (this.classList.contains('expanded')) {
        this.classList.remove('expanded');
    } else {
        this.classList.add('expanded');
    }
};

var thumbs = document.getElementsByTagName("img");
for (var i = 0; i < thumbs.length; i++) {
    var n = Number(thumbs[i].getAttribute("width"));
    if (n > 620) {
        thumbs[i].addEventListener('click', toggle_thumb, false);
        thumbs[i].classList.add('expandable');
        thumbs[i].removeAttribute('height');
    }
}
